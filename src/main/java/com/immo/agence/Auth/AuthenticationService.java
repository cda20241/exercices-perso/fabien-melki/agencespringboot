package com.immo.agence.Auth;

import com.immo.agence.config.JwtService;
import com.immo.agence.entity.PersonneEntity;
import com.immo.agence.repository.PersonneRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;



@Service
@RequiredArgsConstructor
public class AuthenticationService {

    // injection du repository de la classe User
    private final PersonneRepository personneRepository;
    private final PasswordEncoder passwordEncoder;

    private final JwtService jwtService;

    private final AuthenticationManager authenticationManager;

    // méthode d'enregistrement d'un nouvel utilisateur
    public PersonneEntity register(PersonneEntity request) {
        var user = PersonneEntity.builder()
                .nom(request.getNom())
                .prenom(request.getPrenom())
                .email(request.getEmail())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(request.getRole())
                .rue(request.getRue())
                .ville(request.getVille())
                .codePostal(request.getCodePostal())
                .build();
        user = personneRepository.save(user);
        if (user == null) {
            return null;
        }
        return user;
    }


    // méthode d'authentification d'un nouvelle utilisateur
    public AuthentificationResponse authenticate(AuthenticationRequest request) {
        authenticationManager.authenticate(
                // génération du token d'authentification de type username and passmord
                new UsernamePasswordAuthenticationToken(
                        request.getEmail(),
                        request.getPassword()
                )
        );
        // si le mail et la mot de passe sont corrects on recherche le client dans la bdd
        var user = personneRepository.findUserByEmail(request.getEmail())
                .orElseThrow();
        // génération du token de l'utilisateur
        var jwtToken = jwtService.generateToken(user);
        return AuthentificationResponse.builder()
                // on passe le token à la réponse que nous renvoyons au client
                .role(user.getRole())
                .isLogged(true)
                .token(jwtToken)
                .build();
    }
}