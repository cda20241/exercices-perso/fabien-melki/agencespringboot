package com.immo.agence.Auth;
import com.immo.agence.enums.RoleEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AuthentificationResponse {

    private RoleEnum role;
    private String token;
    private Boolean isLogged;
}