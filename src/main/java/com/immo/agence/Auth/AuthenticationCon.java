package com.immo.agence.Auth;

import com.immo.agence.entity.PersonneEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:4200")
public class AuthenticationCon {

    private final AuthenticationService authenticationService;

    @PostMapping("/register")
    public PersonneEntity register(
            @RequestBody PersonneEntity request
    ) {
        return authenticationService.register(request);
    }

    @PostMapping("/authenticate")
    public AuthentificationResponse authenticate (
            @RequestBody AuthenticationRequest request
    ) {
        return authenticationService.authenticate(request);
    }
}