package com.immo.agence.dto;

import com.immo.agence.entity.BienEntity;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Set;

@Data
@RequiredArgsConstructor

public class BaseBienDTO {
    private Long id;
    private String ville;


    public void mapper(BienEntity bienEntity){
        this.id = bienEntity.getId();
        this.ville = bienEntity.getVille();
    }

}
