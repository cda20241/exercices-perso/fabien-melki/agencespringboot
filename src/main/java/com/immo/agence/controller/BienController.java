package com.immo.agence.controller;

//import com.example.AgenceImmo.dto.BaseBienDTO;
import com.immo.agence.entity.BienEntity;
import com.immo.agence.enums.TypeEnum;
import com.immo.agence.repository.BienRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("bien")
public class BienController {

    @Autowired

    private final BienRepository bienRepository;


    public BienController(BienRepository bienRepository) {
        this.bienRepository = bienRepository;
    }

    @GetMapping("/all")
    public List<BienEntity> getAllBien() {
        return bienRepository.findAll();
    }



    @GetMapping("/{id}")
    public ResponseEntity<?> getBien(@PathVariable Long id) {
        Optional<BienEntity> bien = bienRepository.findById(id);
        if (bien.isPresent()) {
            return ResponseEntity.ok(bien.get());
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Cette id ne correspond à aucun bien.");
        }
    }
    @PostMapping("/ajouter")
    public ResponseEntity<String> addBien(@RequestBody BienEntity bien) {
        bien.setId(null);
        this.bienRepository.save(bien);
        return ResponseEntity.status(HttpStatus.CREATED).body("Le bien a été ajouté à la base de données.");
    }

    @PutMapping("/modifier/{id}")
    public ResponseEntity<?> updateBien(@PathVariable Long id, @RequestBody BienEntity bien) {
        Optional<BienEntity> bienOptional = bienRepository.findById(id);
        if (bienOptional.isPresent()) {
            bien.setId(id);
            BienEntity updatedBien = bienRepository.save(bien);
            return ResponseEntity.ok(updatedBien);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Cette id ne correspond à aucun bien.");
        }
    }
    @DeleteMapping("/supprimer/{id}")
    public ResponseEntity<String> deleteBien(@PathVariable Long id) {
        Optional<BienEntity> bien = this.bienRepository.findById(id);
        if (bien.isPresent()) {
            this.bienRepository.deleteById(id);
            return new ResponseEntity<>("Ce bien a été supprimé de la base de données", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Ce bien n'est pas présent dans la base de données", HttpStatus.NOT_FOUND);
        }
    }
    @GetMapping("/filtre/{type}")
    public List<BienEntity> filtreBien(@PathVariable String type) {
        type = type.toUpperCase();
        return bienRepository.findByType(TypeEnum.valueOf(type));
    }
}


