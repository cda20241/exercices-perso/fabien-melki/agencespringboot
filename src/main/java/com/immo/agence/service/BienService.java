package com.immo.agence.service;

import com.immo.agence.repository.BienRepository;
import com.immo.agence.repository.PersonneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BienService {
    @Autowired
    private BienRepository bienRepository;
    private PersonneRepository personneRepository;

    public BienService(BienRepository bienRepository, PersonneRepository personneRepository) {
        this.bienRepository = bienRepository;
        this.personneRepository = personneRepository;
    }


}
