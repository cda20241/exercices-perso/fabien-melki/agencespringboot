package com.immo.agence.service;

import com.immo.agence.dto.BaseBienDTO;
import com.immo.agence.dto.PersonneDTO;
import com.immo.agence.entity.BienEntity;
import com.immo.agence.entity.PersonneEntity;
import com.immo.agence.repository.BienRepository;
import com.immo.agence.repository.PersonneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PersonneService {
    @Autowired
    private BienRepository bienRepository;
    private PersonneRepository personneRepository;

    private PersonneEntity personneEntity;

    public PersonneService(BienRepository bienRepository, PersonneRepository personneRepository) {
        this.bienRepository = bienRepository;
        this.personneRepository = personneRepository;
    }

    public PersonneEntity getInfoFiltree(Long id) {
        if (personneRepository.findById(id).isPresent()) {
            PersonneEntity personne = personneRepository.findById(id).get();
            return personne;
        }
        return null;
    }

    public ResponseEntity<String> ajouterPersonneBien(Long id_personne, Long id_bien) {
        Optional<PersonneEntity> personneOptional = personneRepository.findById(id_personne);
        Optional<BienEntity> bienOptional = bienRepository.findById(id_bien);

        if (personneOptional.isPresent() && bienOptional.isPresent()) {
            PersonneEntity personneEntity = personneOptional.get();
            BienEntity bienEntity = bienOptional.get();

            List<PersonneEntity> personnes = bienEntity.getPersonnes();
            if (personnes.contains(personneEntity)) {
                return new ResponseEntity<>("Cette personne a un lien avec un bien", HttpStatus.ALREADY_REPORTED);
            } else {
                personnes.add(personneEntity);
                bienEntity.setPersonnes(personnes);
                bienRepository.save(bienEntity);
                return new ResponseEntity<>(personneEntity.getNom() + " est maintenant " + personneEntity.getRole() + " de ce bien", HttpStatus.OK);
            }
        } else {
            return new ResponseEntity<>("Personne ou bien introuvable", HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<String> supprimerPersonneBien(Long id_personne, Long id_bien) {
        Optional<PersonneEntity> personneOptional = personneRepository.findById(id_personne);
        Optional<BienEntity> bienOptional = bienRepository.findById(id_bien);
        if (personneOptional.isPresent() && bienOptional.isPresent()) {
            PersonneEntity personneEntity = personneOptional.get();
            BienEntity bienEntity = bienOptional.get();
            List<PersonneEntity> personnes = bienEntity.getPersonnes();
            if (personnes.contains(personneEntity)) {
                personnes.remove(personneEntity);
                bienEntity.setPersonnes(personnes);
                bienRepository.save(bienEntity);
                return new ResponseEntity<>(personneEntity.getNom() + " est retirée " + personneEntity.getRole() + " de ce bien", HttpStatus.OK);
            } else {
                return new ResponseEntity<>("Cette personne ne posséde pas ce bien", HttpStatus.NOT_FOUND);
            }
        } else {
            return new ResponseEntity<>("Personne ou bien introuvable", HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<PersonneDTO> trouverBiensParIdPersonne(PersonneEntity personne) {
        // Je récupère tous les biens de la base de données
        List<BienEntity> tousLesBiens = bienRepository.findAll();
        // je crée une liste pour stocker les biens de la personne
        List<BaseBienDTO> biensDeLaPersonne = new ArrayList<>();
        // on parcourt tous les biens pour filtrer ceux qui appartiennent à la personne
        for (BienEntity bien : tousLesBiens) {
            // on vérifie si la liste des personnes associées à ce bien contient la personne spécifiée
            if (bien.getPersonnes().contains(personne)) {
                // J'instancie ma classe BienDTO pour applique le filtre DTO souhaité BasebienDTO ne donne que l'ID et la ville
                BaseBienDTO bienDTO = new BaseBienDTO();
                //je passe mon bien dans la fonction mapper de ma classe bienDTO qui ne me donne une surcouche DTO en m'ajoutant la liste des biens
                bienDTO.mapper(bien);
                //j'ajoute le résultat obtenu à ma liste biensDeLaPersonne
                biensDeLaPersonne.add(bienDTO);
            }
        }
        //J'instancie maintenant la DTO personne qui ne me donnera que l'ID, le nom et le prénom
        PersonneDTO result = new PersonneDTO();
        //Comme précédemment, je passe au mapper pour ajouter une liste de biens. Il fallait une liste de bien DTO
        // car c'était ce qui était demandé comme paramètre de mon mapper dans PersonneDTO
        result.mapper(personne, biensDeLaPersonne);

        // Retour de la liste des biens de la personne
        return ResponseEntity.ok().body(result);
    }


}




