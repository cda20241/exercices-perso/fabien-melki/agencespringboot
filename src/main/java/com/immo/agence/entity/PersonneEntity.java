package com.immo.agence.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.immo.agence.enums.RoleEnum;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;


import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Data
@Builder
@RequiredArgsConstructor
@AllArgsConstructor


@Table(name="personne")
//AJout le JsonIgnore pour ne pas que ces données soient envoyées
@JsonIgnoreProperties({"accountNonExpired", "accountNonLocked", "credentialsNonExpired"})

public class PersonneEntity implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_personne", nullable = false, updatable = false)
    private Long id;

    @Column
    private String nom;
    private String prenom;
    private String rue;
    private String ville;
    private String codePostal;
    private String password;

    @Column(unique = true)
    private String email;

    @Enumerated(EnumType.STRING) @Column(name = "role",  nullable = false)
    private RoleEnum role;

    public void setBiens(List<BienEntity> list) {
    }
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority(role.name()));
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}