package com.immo.agence.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.immo.agence.enums.TypeEnum;
import jakarta.persistence.*;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Data
@RequiredArgsConstructor
@Table(name="bien")
public class BienEntity {
    @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name= "id_bien", nullable = false, updatable = false)
        private Long id;

    @Column private String rue;
    @Column private String ville;
    @Column private Integer codePostal;

    @Enumerated(EnumType.STRING)
    private TypeEnum type;

    @ManyToMany()
    @JoinTable(name="personneBien",
        joinColumns = @JoinColumn(name ="id_bien", referencedColumnName = "id_bien"),
        inverseJoinColumns = @JoinColumn(name ="id_personne", referencedColumnName = "id_personne"))
    private List<PersonneEntity> personnes = new ArrayList<>();

}
