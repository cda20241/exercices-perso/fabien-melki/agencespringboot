package com.immo.agence.repository;

import com.immo.agence.entity.BienEntity;
import com.immo.agence.enums.TypeEnum;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BienRepository extends JpaRepository<BienEntity, Long> {
    List<BienEntity> findByType(TypeEnum type);


}
